package com.booking.models;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class Reservation {
    private String reservationId;
    private Customer customer;
    private Employee employee;
    private List<Service> services;
    private double reservationPrice;
    private String workstage;
    //   workStage (In Process, Finish, Canceled)

    public Reservation(String reservationId, Customer customer, Employee employee, List<Service> services,
            String workstage) {
        this.reservationId = reservationId;
        this.customer = customer;
        this.employee = employee;
        this.services = services;
        this.reservationPrice = calculateReservationPrice();
        this.workstage = workstage;
    };



    private double calculateReservationPrice() {
        double totalBiaya = 0;
        double diskon = 0;
        for (Service service : services) {
            totalBiaya += service.getPrice();
        }
        switch (customer.getMember().getMembershipName()) {
            case "None":
                diskon = 0;
                break;
            case "Silver":
                diskon = 0.05;
                break;
            case "Gold":
                diskon = 0.10; 
                break;
        }

        totalBiaya -= totalBiaya * diskon;
        return totalBiaya;
    }
}
