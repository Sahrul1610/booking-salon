package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;
import com.booking.repositories.PersonRepository;
import com.booking.repositories.ServiceRepository;

public class MenuService {
    private static List<Person> personList = PersonRepository.getAllPerson();
    private static List<Service> serviceList = ServiceRepository.getAllService();
    private static List<Reservation> reservationList = new ArrayList<>();
    private static Scanner input = new Scanner(System.in);

    public static void mainMenu() {
        String[] mainMenuArr = { "Show Data", "Create Reservation", "Complete/cancel reservation", "Exit" };
        String[] subMenuArr = { "Recent Reservation", "Show Customer", "Show Available Employee", "Show History",
                "Back to main menu" };

        int optionMainMenu;
        int optionSubMenu;

        boolean backToMainMenu = false;
        boolean backToSubMenu = false;
        do {
            PrintService.printMenu("Main Menu", mainMenuArr);
            String mainMenuInput = input.nextLine();
            if (ValidationService.isValidNumber(mainMenuInput)) {
                optionMainMenu = Integer.valueOf(mainMenuInput);
                switch (optionMainMenu) {
                    case 1:
                        backToSubMenu = true;
                        do {
                            PrintService.printMenu("Show Data", subMenuArr);
                            String subMenuInput = input.nextLine();
                            if (ValidationService.isValidNumber(subMenuInput)) {
                                optionSubMenu = Integer.valueOf(subMenuInput);
                                // Sub menu - menu 1
                                switch (optionSubMenu) {
                                    case 1:
                                        // panggil fitur tampilkan recent reservation
                                        PrintService.showRecentReservation(reservationList, true);
                                        break;
                                    case 2:
                                        // panggil fitur tampilkan semua customer
                                        PrintService.showAllCustomer(personList);
                                        break;
                                    case 3:
                                        // panggil fitur tampilkan semua employee
                                        PrintService.showAllEmployee(personList);
                                        break;
                                    case 4:
                                        // panggil fitur tampilkan history reservation + total keuntungan
                                        PrintService.showHistoryReservation(reservationList);
                                        break;
                                    case 0:
                                        backToSubMenu = true;
                                }
                            } else {
                                System.out.println("Input tidak valid. Masukkan nomor.\n");
                            }
                        } while (!backToSubMenu);
                        break;
                    case 2:
                        // panggil fitur menambahkan reservation
                        ReservationService.createReservation(reservationList, personList, serviceList);
                        break;
                    case 3:
                        // panggil fitur mengubah workstage menjadi finish/cancel
                        ReservationService.editReservationWorkstage(reservationList);
                        break;
                    case 0:
                        backToMainMenu = true;
                        break;
                }
            } else {
                System.out.println("Input tidak valid. Masukkan nomor.\n");
            }
        } while (!backToMainMenu);

    }
}
