package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    private static Scanner scanner = new Scanner(System.in);

    public static void createReservation(List<Reservation> reservationList, List<Person> personList,
            List<Service> serviceList) {
        System.out.println("Membuat Reservasi\n");

        PrintService.showAllCustomer(personList);
        if (personList.isEmpty()) {
            System.out.println("Tidak ada pelanggan tersedia. Silakan tambahkan pelanggan terlebih dahulu.");
            return;
        }

        System.out.println("\nSilahkan Masukkan Customer Id:");
        String customerId = scanner.nextLine();
        Customer customer = ValidationService.validateCustomerId(personList, customerId, scanner);
        PrintService.showAllEmployee(personList);
        if (personList.isEmpty()) {
            System.out.println("Tidak ada karyawan tersedia. Silakan tambahkan karyawan terlebih dahulu.");
            return;
        }

        System.out.println("\nSilahkan Masukkan Employee Id");
        String employeeId = scanner.nextLine();
        Employee employee = ValidationService.validateEmployeeById(personList, employeeId, scanner);
        PrintService.showAllServices(serviceList);
        if (serviceList.isEmpty()) {
            System.out.println("Tidak ada layanan tersedia. Silakan tambahkan layanan terlebih dahulu.");
            return;
        }

        // double totalBiaya = 0;
        List<Service> selectedServices = new ArrayList<>();
        String choice = "Y"; 
        boolean continueSelecting = true;
        do {
            System.out.println("\nSilahkan Masukkan Service Id :");
            String serviceId = scanner.nextLine();
            if (serviceId.equalsIgnoreCase("T")) {
                continueSelecting = false; 
                break;
            }
            Service selectedService = ValidationService.validateServiceById(serviceList, serviceId, selectedServices);
            if (selectedService != null) {
                selectedServices.add(selectedService);
            } else {
                System.out.println("ID layanan tidak valid.");
                continue; 
            }
            System.out.println("Ingin pilih service yang lain (Y/T)?");
            choice = scanner.nextLine();
        } while (continueSelecting && choice.equalsIgnoreCase("Y"));

        String reservationId = String.format("Rsv-%02d", reservationList.size() + 1);
        Reservation reservation = new Reservation(reservationId, customer, employee, selectedServices,
                "In process");
        reservationList.add(reservation);

        System.out.println("Booking Berhasil!");
        System.out.println("Total Biaya Booking: Rp" + reservation.getReservationPrice());
    }

    public static void editReservationWorkstage(List<Reservation> reservationList) {
        boolean isValidReservationId;
        String reservationId;
        Reservation reservation;
    
        PrintService.showRecentReservation(reservationList, false);
        System.out.println("0. Kembali ke Main Menu\n");
        do {
            System.out.println("Silahkan Masukkan Reservation Id:");
            reservationId = scanner.nextLine();
            if (reservationId.equals("0")) {
                System.out.println("Kembali ke menu utama.");
                return;
            }
            reservation = findReservationById(reservationList, reservationId);
            isValidReservationId = reservation != null;
            if (!isValidReservationId) {
                System.out.println("Reservasi dengan ID tersebut tidak ditemukan.");
            } else if (!reservation.getWorkstage().equalsIgnoreCase("In Process")) {
                System.out.println("Reservasi dengan ID tersebut sudah selesai.");
                isValidReservationId = false;
            }
        } while (!isValidReservationId);
    
        System.out.println("Selesaikan reservasi (Finish/Cancel):");
        String choice = scanner.nextLine();
        switch (choice.toLowerCase()) {
            case "finish":
                reservation.setWorkstage("Finish");
                System.out.println("Reservasi dengan id " + reservationId + " sudah Finish.");
                break;
            case "cancel":
                reservation.setWorkstage("Canceled");
                System.out.println("Reservasi dengan id " + reservationId + " sudah Cancel.");
                break;
            default:
                System.out.println("Pilihan tidak valid.");
        }
    }
    

    private static Reservation findReservationById(List<Reservation> reservationList, String reservationId) {
        for (Reservation reservation : reservationList) {
            if (reservation.getReservationId().equals(reservationId)) {
                return reservation;
            }
        }
        return null;
    }

    public static Customer getCustomerByCustomerId(List<Person> personList, String customerId) {
        for (Person person : personList) {
            if (person instanceof Customer && person.getId().equals(customerId)) {
                return (Customer) person;
            }
        }
        return null;
    }

    public static Employee findEmployeeById(List<Person> personList, String employeeId) {
        for (Person person : personList) {
            if (person instanceof Employee && person.getId().equals(employeeId)) {
                return (Employee) person;
            }
        }
        return null;
    }

}
