package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {
    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        String result = "";
        // Bisa disesuaikan kembali
        for (Service service : serviceList) {
            result += service.getServiceName() + ", ";
        }
        return result;
    }

    public static void showRecentReservation(List<Reservation> reservationList, boolean showWorkstage) {
        System.out.println(
                "+===========================================================================================================+");
        System.out.printf("| %-4s | %-6s | %-15s | %-40s | %-12s |",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya");
        if (showWorkstage) {
            System.out.printf(" | %-10s |\n", "Workstage");
        } else {
            System.out.println();
        }
        System.out.println(
                "+===========================================================================================================+");
        int num = 1;
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("In Process")) {
                StringBuilder services = new StringBuilder();
                List<Service> reservationServices = reservation.getServices();
                for (int i = 0; i < reservationServices.size(); i++) {
                    Service service = reservationServices.get(i);
                    services.append(service.getServiceName());
                    if (i < reservationServices.size() - 1) {
                        services.append(", ");
                    }
                }
                if (showWorkstage) {
                    System.out.printf("| %-4d | %-6s | %-15s | %-40s | %-12.2f | %-10s |\n",
                            num, reservation.getReservationId(), reservation.getCustomer().getName(),
                            services.toString(),
                            reservation.getReservationPrice(), reservation.getWorkstage());
                } else {
                    System.out.printf("| %-4d | %-6s | %-15s | %-40s | %-12.2f |\n",
                            num, reservation.getReservationId(), reservation.getCustomer().getName(),
                            services.toString(),
                            reservation.getReservationPrice());
                }
                num++;
            }
        }
        System.out.println(
                "+===========================================================================================================+");
    }

    public static void showAllCustomer(List<Person> personList) {
        System.out.println("+=============================================================================+");
        System.out.printf("| %-4s | %-7s | %-10s | %-15s | %-10s | %-10s |\n",
                "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out.println("+=============================================================================+");
        int num = 1;
        for (Person person : personList) {
            if (person instanceof Customer) {
                Customer customer = (Customer) person;
                System.out.printf("| %-4d | %-7s | %-10s | %-15s | %-10s | Rp%-9.2f |\n",
                        num, customer.getId(), customer.getName(), customer.getAddress(),
                        customer.getMember().getMembershipName(), customer.getWallet());
                num++;
            }
        }
        System.out.println("+=============================================================================+");
    }

    public static void showAllEmployee(List<Person> personList) {
        System.out.println("+==============================================================+");
        System.out.printf("| %-4s | %-7s | %-10s | %-15s | %-10s |\n",
                "No.", "ID", "Nama", "Alamat", "Pengalaman");
        System.out.println("+==============================================================+");
        int num = 1;
        for (Person person : personList) {
            if (person instanceof Employee) {
                Employee employee = (Employee) person;
                System.out.printf("| %-4d | %-7s | %-10s | %-15s | %-10d |\n",
                        num, employee.getId(), employee.getName(), employee.getAddress(), employee.getExperience());
                num++;
            }
        }
        System.out.println("+==============================================================+");
    }

    public static void showAllServices(List<Service> serviceList) {
        System.out.println("+==============================================================================+");
        System.out.printf("| %-4s | %-8s | %-20s | %-10s |\n", "No.", "ID", "Nama", "Harga");
        System.out.println("+==============================================================================+");
        for (int i = 0; i < serviceList.size(); i++) {
            Service service = serviceList.get(i);
            System.out.printf("| %-4d | %-8s | %-20s | Rp%-8.2f |\n", (i + 1), service.getServiceId(),
                    service.getServiceName(), service.getPrice());
        }
        System.out.println("+==============================================================================+");
    }

    public static void showHistoryReservation(List<Reservation> reservationList) {
        System.out.println("Histori Reservasi");
        System.out.println(
                "+=============================================================================================================+");
        System.out.printf("| %-4s | %-6s | %-15s | %-40s | %-12s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out.println(
                "+=============================================================================================================+");
        int num = 1;
        double totalKeuntungan = 0;
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")
                    || reservation.getWorkstage().equalsIgnoreCase("Canceled")) {
                StringBuilder services = new StringBuilder();
                List<Service> reservationServices = reservation.getServices();
                for (int i = 0; i < reservationServices.size(); i++) {
                    Service service = reservationServices.get(i);
                    services.append(service.getServiceName());
                    if (i < reservationServices.size() - 1) {
                        services.append(", ");
                    }
                }

                double totalBiaya = 0;
                if (reservation.getWorkstage().equalsIgnoreCase("Finish")) {
                    totalBiaya = reservation.getReservationPrice();
                    totalKeuntungan += totalBiaya;
                }

                System.out.printf("| %-4d | %-6s | %-15s | %-40s | %-12.2f | %-10s |\n",
                        num, reservation.getReservationId(), reservation.getCustomer().getName(), services.toString(),
                        totalBiaya, reservation.getWorkstage());
                num++;
            }
        }
        System.out.println(
                "+=============================================================================================================+");
        System.out.printf("Total Keuntungan: Rp %-12.2f%n", totalKeuntungan);
        System.out.println(
                "+=============================================================================================================+");
    }

}
