package com.booking.service;

import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Service;

public class ValidationService {


    public static Customer validateCustomerId(List<Person> personList, String customerId, Scanner scanner) {
        Customer customer = ReservationService.getCustomerByCustomerId(personList, customerId);
        while (customer == null) {
            System.out.println("Employee yang di cari tidak tersedia");
            System.out.println("Silakan masukkan kembali Customer Id:");
            customerId = scanner.nextLine();
            customer = ReservationService.getCustomerByCustomerId(personList, customerId);
        }
        return customer;
    }


    public static Employee validateEmployeeById(List<Person> personList, String employeeId, Scanner scanner) {
        Employee employee = ReservationService.findEmployeeById(personList, employeeId);
        while (employee == null) {
            System.out.println("Employee yang di cari tidak tersedia");
            System.out.println("Silakan masukkan kembali Employee Id:");
            employeeId = scanner.nextLine();
            employee = ReservationService.findEmployeeById(personList, employeeId);
        }
        return employee;
    }
    

    public static Service validateServiceById(List<Service> services, String serviceId, List<Service> selectedServices) {
        for (Service service : services) {
            if (service.getServiceId().equals(serviceId)) {
                if (selectedServices.contains(service)) {
                    System.out.println("Service sudah dipilih.");
                    return null;
                } else {
                    return service;
                }
            }
        }
        System.out.println("Service yang dicari tidak tersedia.");
        return null;
    }
    

    public static boolean isValidNumber(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
